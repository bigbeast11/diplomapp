﻿using DiplomApp.v2.Interfaces;
using DiplomApp.v2.Models;
using DiplomApp.v2.Services;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Autofac;

namespace DiplomApp.v2.ViewModels
{
    public class HomePageViewModel : IViewModel, INotifyPropertyChanged
    {
        public Command SetAllParametersCommand { get; set; }
        IDevice _deviceService;

        #region fields
        //string error;
        bool alarmIsOn;
        byte aot;
        string hourWeekend;
        string minuteWeekend;
        string hourDays;
        string minuteDays;

        bool _isError;
        bool isBusy;
        byte brightnessValue;
        byte brightnessMinValue;
        byte brightnessMaxValue;

        byte brightnessNightValue;
        byte brightnessNightMinValue;
        byte brightnessNightMaxValue;
        #endregion

        #region Propertys
        public bool IsError
        {
            get => _isError;
            set
            {
                if (_isError == value) return;
                _isError = value;
            }
        }
        public string HourDays
        {
            get { return hourDays; }
            set
            {
                hourDays = value;
                OnPropertyChanged();
            }
        }
        public string MinuteDays
        {
            get { return minuteDays; }
            set
            {
                minuteDays = value;
                OnPropertyChanged();
            }
        }
        public string HourWeekend
        {
            get { return hourWeekend; }
            set
            {
                hourWeekend = value;
                OnPropertyChanged();
            }
        }
        public string MinuteWeekend
        {
            get { return minuteWeekend; }
            set
            {
                minuteWeekend = value;
                OnPropertyChanged();
            }
        }
        public byte BrightnessMaxValue
        {
            get { return brightnessMaxValue; }
            set
            {
                brightnessMaxValue = value;
                OnPropertyChanged();
            }
        }

        public byte AOT
        {
            get
            {
                return aot;
            }
            set
            {
                aot = value;
                OnPropertyChanged();
            }
        }
        public byte BrightnessMinValue
        {
            get { return brightnessMinValue; }
            set
            {
                brightnessMinValue = value;
                OnPropertyChanged();
            }
        }

        public byte BrightnessNightMaxValue
        {
            get { return brightnessMaxValue; }
            set
            {
                brightnessNightMaxValue = value;
                OnPropertyChanged();
            }
        }
        public byte BrightnessNightMinValue
        {
            get { return brightnessNightMinValue; }
            set
            {
                brightnessNightMinValue = value;
                OnPropertyChanged();
            }
        }
        public bool AlarmIsOn
        {
            get
            {
                return alarmIsOn;
            }
            set
            {
                alarmIsOn = value;

                OnPropertyChanged();
            }
        }

        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;

                OnPropertyChanged();
                SetAllParametersCommand?.ChangeCanExecute();
            }
        }

        public byte BrightnessValue
        {
            get { return brightnessValue; }
            set
            {
                brightnessValue = value;
                OnPropertyChanged();
            }
        }

        public byte BrightnessNightValue
        {
            get { return brightnessNightValue; }
            set
            {
                brightnessNightValue = value;
                OnPropertyChanged();
            }
        }
        #endregion

        static private HomePageViewModel @default;

        static public HomePageViewModel Default
        {//Решить заглушку
            get { return @default ?? (@default = new HomePageViewModel(null)); }
        }

        public void SetBrightnessParameters(byte max, byte min)
        {
            BrightnessMaxValue = max;
            BrightnessMinValue = min;
            BrightnessValue = (byte)((max + min) / 2);

            BrightnessNightMaxValue = max;
            BrightnessNightMinValue = min;
            BrightnessNightValue = (byte)((max + min) / 2);
        }
        public async void ShowError(string error)
        {
            IsError = true;
            await Application.Current.MainPage.DisplayAlert("Ошибка", error, "OK");
            return;
        }
        public HomePageViewModel(IDevice deviceService)
        {
            _deviceService = deviceService;
            IsBusy = true;
            IsError = false;
            BrightnessMaxValue = 100;
            BrightnessMinValue = 20;
            BrightnessValue = 50;

            BrightnessNightMaxValue = 70;
            BrightnessNightMinValue = 0;
            BrightnessNightValue = 30;

            AlarmIsOn = false;

            SetAllParametersCommand = new Command(async () => await SetAllParameters(),
                () => !IsBusy);
        }

        private async Task SetAllParameters()
        {
            try
            {
                if (IsError) IsError = false;

                BluetoothConnection btConnect = new BluetoothConnection();

                IsBusy = true;

                //Здесь писать код отправки команд

                _deviceService.SendCommand(new DeviceCommand<CommandModel>() { CommandName = ""});

                await Task.Delay(1000);

                IsBusy = false;

                if (!IsError)
                    await Application.Current.MainPage.DisplayAlert("Отлично", "Данные отправлены", "OK");
            }
            catch (Exception e)
            {

                Default.ShowError(e.Message);
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

    }
}
