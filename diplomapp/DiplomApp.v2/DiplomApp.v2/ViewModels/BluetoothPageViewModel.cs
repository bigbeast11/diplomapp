﻿using Android.Bluetooth;
using DiplomApp.v2.Models;
using DiplomApp.v2.Services;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace DiplomApp.v2.ViewModels
{
    public class BluetoothPageViewModel : IViewModel, INotifyPropertyChanged
    {
        BluetoothConnection btAdress;
        BluetoothAdapter btAdapter;

        public ObservableCollection<BtDeviceModel> btDevicesCollection { get; set; } =
            new ObservableCollection<BtDeviceModel>();
        public ICommand ConnectToDeviceCommand { get; set; }
        public BluetoothPageViewModel()
        {
            IsBusy = false;
            btAdapter = BluetoothAdapter.DefaultAdapter;
            btAdress = new BluetoothConnection();
            ConnectToDeviceCommand = new Command((x) =>ConnectToDevice(x));
            ShowBondedCommand = new Command(() => ShowBondedMethod());
            btConnection();
        }

        public Command ShowBondedCommand { get; set; }

        bool _bluetoothSwitch;
        public bool BluetoothSwitch
        {
            get { return _bluetoothSwitch; }
            set
            {
                _bluetoothSwitch = value;

                btDevicesCollection.Clear();
                OnPropertyChanged();
            }
        }
        bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }
        private void ShowBondedMethod()
        {
            btDevicesCollection.Clear();
            if (btAdapter.IsEnabled)
            {
                IsBusy = true;
                btAdapter.StartDiscovery();

                foreach (var device in btAdapter.BondedDevices)
                {
                    btDevicesCollection.Add(new BtDeviceModel() { DisplayName = device.Name, DisplayAdress = device.Address });
                }
                IsBusy = false;
            }
            btAdapter.CancelDiscovery();
        }

        private void btConnection()
        {

            if (btAdapter.IsEnabled)
            {
                BluetoothSwitch = true;
            }
            else
            {
                BluetoothSwitch = false;
            }
        }

        static private BluetoothPageViewModel @default;

        static public BluetoothPageViewModel Default
        {
            get { return @default ?? (@default = new BluetoothPageViewModel()); }
        }



        private void ConnectToDevice(object element)
        {
            try
            {
                var adress = (element as BtDeviceModel).DisplayAdress;

                if (adress != null)
                {
                    var result = btAdress.Connect(adress);
                    if (result)
                    {
                        HomePageViewModel.Default.IsBusy = false;
                        ThinSettingsPageViewModel.Default.IsBusy = false;
                        Application.Current.MainPage.DisplayAlert("Отлично", $"Подключено к " +
                            $"{(element as BtDeviceModel).DisplayName}", "OK");
                    }
                    else HomePageViewModel.Default.ShowError("Ошибка соединения");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
            
            
        }
        void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
