﻿using DiplomApp.v2.Services;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DiplomApp.v2.ViewModels
{
    public class ThinSettingsPageViewModel : IViewModel, INotifyPropertyChanged
    {
        BluetoothConnection btConnect = new BluetoothConnection();
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        static ThinSettingsPageViewModel @default;
        public static ThinSettingsPageViewModel Default
        {
            get
            {
                return @default ?? (@default = new ThinSettingsPageViewModel());
            }
        }

        byte maximumBrightness;
        byte minimumBrightness;
        byte tapeTemperature;
        byte maxTemp;
        byte minTemp;

        byte switchOffType;
        byte switchOffTemp;
        byte tapeCountDown;

        int comboboxIndex;
        bool isBusy;

        public byte TapeCountDown
        {
            get => tapeCountDown;
            set { tapeCountDown = value; }
        }
        public byte MaxTemp
        {
            get { return maxTemp; }
            set
            {
                maxTemp = value;
                OnPropertyChanged();
            }
        }
        public byte MinTemp
        {
            get { return minTemp; }
            set
            {
                minTemp = value;
                OnPropertyChanged();
            }
        }
        public bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;

                OnPropertyChanged();
                try
                {
                    SetParameters?.ChangeCanExecute();
                }
                catch (Exception ex)
                {

                    throw;
                }
                
            }
        }
        public int ComboboxIndex
        {
            get { return comboboxIndex; }
            set
            {
                comboboxIndex = value;
                OnPropertyChanged();
            }
        }

        public byte SwitchOffType
        {
            get
            {
                return switchOffType;
            }
            set
            {
                switchOffType = value;
                OnPropertyChanged();
            }
        }
        public byte SwitchOffTemp
        {
            get
            {
                return switchOffTemp;
            }
            set
            {
                switchOffTemp = value;
                OnPropertyChanged();
            }
        }

        public byte TapeTemperature
        {
            get
            {
                return tapeTemperature;
            }
            set
            {
                tapeTemperature = value;
                OnPropertyChanged();
            }
        }

        public Command SetParameters { get; set; }

        public byte MaximumBrightness
        {
            get { return maximumBrightness; }
            set
            {
                maximumBrightness = value;
                OnPropertyChanged();
            }
        }

        public byte MinimumBrightness
        {
            get { return minimumBrightness; }
            set
            {
                minimumBrightness = value;
                OnPropertyChanged();
            }
        }
        private void SendParameters()
        {
            if (MaximumBrightness > MinimumBrightness)
            {
                HomePageViewModel.Default.SetBrightnessParameters(MaximumBrightness, MinimumBrightness);

                btConnect.writeDataToArd(string.Format("LLM:{0}", MaximumBrightness.ToString()));
                btConnect.writeDataToArd(string.Format("LLI:{0}", MinimumBrightness.ToString()));
            }
            else
                Application.Current.MainPage.DisplayAlert("Ошибка", "Максимальная яркость должна быть больше минимальной", "OK");

            btConnect.writeDataToArd(string.Format("TML:{0}", TapeTemperature.ToString()));
            btConnect.writeDataToArd(string.Format("AOM:{0}", ComboboxIndex));
            btConnect.writeDataToArd($"TMO:{SwitchOffTemp}");
            btConnect.writeDataToArd($"TMS:{TapeCountDown}");

            Task.Delay(1000);

            //TODO:добавить температуру отключения
        }
        public ThinSettingsPageViewModel()
        {
            MaxTemp = 100;
            MinTemp = 0;
            TapeTemperature = 50;
            IsBusy = true;
            TapeCountDown = 0;
            SwitchOffTemp = 65;
            SetParameters = new Command(() => SendParameters(), ()=> !IsBusy);
        }


    }
}
