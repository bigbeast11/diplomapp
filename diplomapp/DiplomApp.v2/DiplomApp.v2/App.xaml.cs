﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;

using Xamarin.Forms;
using DiplomApp.v2.Views;
using DiplomApp.v2.ViewModels;
using DiplomApp.v2.Infrastructure;
using Infrastructure;
using DiplomApp.v2.Services;
using DiplomApp.v2.Interfaces;

namespace DiplomApp.v2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Autofac.ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<HomePage>();
            builder.RegisterType<HomePageViewModel>();
            builder.RegisterType<BluetoothPage>();
            builder.RegisterType<BluetoothPageViewModel>();
            builder.RegisterType<ThinSettingsPage>();
            builder.RegisterType<ThinSettingsPageViewModel>();
            builder.RegisterType<StartPage>();
            builder.RegisterType<StartPageViewModel>();

            builder.RegisterType<Navigator>().As<INavigator>().SingleInstance();
            builder.RegisterType<ViewViewModelMap>().As<IViewViewModelMap>().SingleInstance();
            builder.RegisterType<ViewProvider>().As<IViewProvider>().SingleInstance();
            builder.RegisterType<ViewModelProvider>().As<IViewModelProvider>().SingleInstance();
            builder.RegisterType<Connection>().As<IConnection>().SingleInstance();
            builder.RegisterType<DeviceConnection>().As<IDevice>().SingleInstance();

            var container = builder.Build();
            var map = container.Resolve<IViewViewModelMap>();

            map.Bind<HomePage, HomePageViewModel>();
            map.Bind<BluetoothPage, BluetoothPageViewModel>();
            map.Bind<ThinSettingsPage, ThinSettingsPageViewModel>();
            map.Bind<StartPage, StartPageViewModel>();

            var mainView = new NavigationPage();
            var navigator = container.Resolve<INavigator>();
            navigator.SetContext(mainView.Navigation);

            try
            {
                var viewModel = navigator.NavigateTo<StartPageViewModel>().Result;
            }
            catch (Exception ex)
            {

                var err = ex.Message;
            }
            

            MainPage = mainView;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
