﻿using DiplomApp.v2.ViewModels;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DiplomApp.v2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThinSettingsPage : ContentPage, IView
    {
        public ThinSettingsPage()
        {
            InitializeComponent();
            BindingContext = ThinSettingsPageViewModel.Default;
        }
        private void offTypePicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            ThinSettingsPageViewModel.Default.ComboboxIndex = offTypePicker.SelectedIndex;
        }
    }
}