﻿using Android.Bluetooth;
using DiplomApp.v2.ViewModels;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DiplomApp.v2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BluetoothPage : ContentPage, IView
    {
        public BluetoothPage()
        {
            InitializeComponent();
            this.BindingContext = BluetoothPageViewModel.Default;
        }

        private void BluetoothSwitcher_Toggled(object sender, ToggledEventArgs e)
        {
            var btSwitch = (sender) as Switch;
            var mBtaAdapter = BluetoothAdapter.DefaultAdapter;

            if (btSwitch.IsToggled)
            {
                if (!mBtaAdapter.IsEnabled)
                {
                    mBtaAdapter.Enable();
                }
            }
            else
            {
                if (mBtaAdapter.IsEnabled)
                {
                    mBtaAdapter.Disable();
                    //BluetoothDevicesList.ItemsSource = null;
                }

            }
        }
    }
}