﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Models
{
    public class CommandModel
    {
        string _commandName;
        string _commandValue;
        public string CommandName
        {
            get => _commandName;
            set
            {
                if (value == _commandName) return;
                _commandName = value;
            }
        }
        public string CommandValue
        {
            get => _commandValue;
            set
            {
                if (value == _commandValue) return;
                _commandValue = value;
            }
        }
    }
}
