﻿using DiplomApp.v2.Interfaces;
using DiplomApp.v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Services
{
    //Bluetooth-device
    public class DeviceConnection : IDevice
    {
        #region fields

        IConnection _connectionService;
        string _deviceAdress;
        bool _isConnect;

        #endregion
        #region prop
        public string DeviceAdress
        {
            get => _deviceAdress;
            set
            {
                if (value == _deviceAdress)
                    return;
                _deviceAdress = value;
            }
        }
        public bool IsConnected
        {
            get => _isConnect;
            set
            {
                if (value == _isConnect)
                    return;
                _isConnect = value;
            }
        }
        #endregion

        public void Connect(string deviceAdress)
        {
            _connectionService.Connect(deviceAdress);
        }

        public void Disconnect()
        {
            _connectionService.Disconnect();
        }
        //Узнать насчет возвращаемого типа
        public void SendCommand<TResponse>(IDeviceCommand<TResponse> command)
        {
            _connectionService.WriteData(command.CommandName);
        }
        public DeviceConnection(IConnection connectionService)
        {
            _connectionService = connectionService;
        }
    }
}
