﻿using Android.App;
using Android.Bluetooth;
using DiplomApp.v2.Interfaces;
using DiplomApp.v2.Models;
using Java.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Services
{
    public class Connection : Activity, IConnection
    {
        #region fields

        IDeviceResponseParser _parser;

        static UUID devUUID = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");
        static BluetoothAdapter mBtAdapter = BluetoothAdapter.DefaultAdapter;
        static BluetoothSocket mSocket = null;
        Stream outStream = null;
        Stream inStream = null;

        bool _isConnect;

        #endregion
        #region prop
        public bool IsConnect
        {
            get => _isConnect;
            set
            {
                if (value == _isConnect)
                    return;
                _isConnect = value;
            }
        }
        #endregion
        public async void Connect(string deviceAdress)
        {
            BluetoothDevice device = mBtAdapter.GetRemoteDevice(deviceAdress);

            try
            {
                mSocket = device.CreateInsecureRfcommSocketToServiceRecord(devUUID);
                await mSocket.ConnectAsync();

                if (mSocket.IsConnected)
                {
                    IsConnect = true;
                    WriteData("GET");

                    _parser.Parse<CommandModel>(ReadData());
                }
            }
            catch (Exception ex)
            {
                mSocket.Close();
                throw;
            }
        }

        public void Disconnect()
        {
            mSocket.Close();
        }

        public string ReadData()
        {
            try
            {
                inStream = mSocket.InputStream;

                string command = "";

                Task.Factory.StartNew(() =>
                {
                    byte[] buffer = new byte[1024];
                    int bytes;
                    while (true)
                    {
                        bytes = inStream.Read(buffer, 0, buffer.Length);
                        if (bytes > 0)
                        {//TODO: решить проблему с потоком в UI.
                            RunOnUiThread(() =>
                            {
                                command = Encoding.UTF8.GetString(buffer, 0, buffer.Length).ToUpper();
                            });
                        }
                    }
                });
                //TODO: разобраться в моменте передачи данных с ардуино. 
                mSocket.Close();
                return command;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void WriteData(string data)
        {
            try
            {
                outStream = mSocket.OutputStream;

                byte[] buffer = Encoding.UTF8.GetBytes(data);

                outStream.Write(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Connection(IDeviceResponseParser parser)
        {
            _parser = parser;
        }
    }
}
