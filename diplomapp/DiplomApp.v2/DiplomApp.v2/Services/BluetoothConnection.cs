﻿using Android.App;
using Android.Bluetooth;
using DiplomApp.v2.ViewModels;
using Java.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DiplomApp.v2.Services
{
    public class BluetoothConnection : Activity
    {
        static UUID devUUID = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");
        static BluetoothAdapter mBtAdapter = BluetoothAdapter.DefaultAdapter;
        static BluetoothSocket mSocket = null;
        Stream outStream = null;
        Stream inStream = null;
        byte _flag = 0;

        public bool Connect(string devAdress)
        {
            BluetoothDevice device = mBtAdapter.GetRemoteDevice(devAdress);
            try
            {
                mSocket = device.CreateInsecureRfcommSocketToServiceRecord(devUUID);
                mSocket.Connect();
                if (mSocket.IsConnected)
                {
                    HomePageViewModel.Default.IsBusy = false;
                    ThinSettingsPageViewModel.Default.IsBusy = false;

                    string date = new TimeConverter().GetDate(DateTime.Now.Year,
                        DateTime.Now.Month, DateTime.Now.Day);

                    int time = DateTime.Now.Hour * 3600 +
                        DateTime.Now.Minute * 60 +
                        DateTime.Now.Second;

                    writeDataToArd("GET");

                    writeDataToArd($"CTD:{date}");
                    writeDataToArd($"CTT:{time}");

                    ReadDataFromArd();
                    return true;
                }
                else return false;

            }
            catch (Exception)
            {
                
                mSocket.Close();
                return false;
            }
        }

        public void writeDataToArd(string dataToSend)
        {
            try
            {
                outStream = mSocket.OutputStream;
            }
            catch (Exception e)
            {
                HomePageViewModel.Default.ShowError(e.Message);
            }
            byte[] buffer = Encoding.UTF8.GetBytes(dataToSend);

            try
            {
                outStream.Write(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
            }
        }

        private void ReadDataFromArd()
        {
            try
            {
                inStream = mSocket.InputStream;
            }
            catch (Exception)
            {

            }
            Task.Factory.StartNew(() =>
            {
                byte[] buffer = new byte[1024];
                int bytes;
                while (true)
                {
                    try
                    {

                        bytes = inStream.Read(buffer, 0, buffer.Length);
                        if (bytes > 0)
                        {
                            RunOnUiThread(() =>
                            {
                                ParseCommandFromArd(Encoding.UTF8.GetString(buffer, 0, buffer.Length).ToUpper());
                            });
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            });
        }

        private void ParseCommandFromArd(string command)
        {
            //Будильник да или нет
            if (command.Contains("AON"))
            {
                if (new Regex(@"AON:([0-1])").Match(command).Groups[1].Value == "0")
                    HomePageViewModel.Default.AlarmIsOn = false;
                else
                    HomePageViewModel.Default.AlarmIsOn = true;
            }
            else
            {
                //Таймер отключения 0-255
                if (command.Contains("AOT"))
                {
                    if (byte.Parse(new Regex(@"AOT:(\d{1,3})").Match(command).Groups[1].Value) > 255)
                    {
                        HomePageViewModel.Default.AOT = 255;

                    }
                    else
                    {
                        if (byte.Parse(new Regex(@"AOT:(\d{1,3})").Match(command).Groups[1].Value) < 0)
                            HomePageViewModel.Default.AOT = 0;
                        else
                            HomePageViewModel.Default.AOT = byte.Parse(new Regex(@"AON:(\d{1,3})").Match(command).Groups[1].Value);
                    }
                }//Время будильника в выходные
                else if (command.Contains("ATS"))
                {
                    var ats = int.Parse(new Regex(@"ATS:(\d{0,4}").Match(command).Groups[1].Value);
                    if (ats <= 1439 || ats >= 0)
                        ParseTimeFromInt("ats" + ats);
                    else
                        HomePageViewModel.Default.ShowError("Time is incorrect!");
                }//Время будильника в будни
                else if (command.Contains("ATW"))
                {
                    var atw = int.Parse(new Regex(@"ATW:(\d{0,4}").Match(command).Groups[1].Value);
                    if (atw <= 1439 || atw >= 0)
                        ParseTimeFromInt("atw" + atw);
                    else
                        HomePageViewModel.Default.ShowError("Time is incorrect!");
                }
                else if (command.Contains("LLE"))
                {//Уровень яркости
                    var max = HomePageViewModel.Default.BrightnessMaxValue;
                    var min = HomePageViewModel.Default.BrightnessMinValue;

                    var lle = byte.Parse(new Regex(@"LLE:(\d{1,3})").Match(command).Groups[1].Value);

                    if (lle > max)
                    {
                        lle = max;
                    }
                    if (lle < min)
                    {
                        lle = min;
                    }

                    HomePageViewModel.Default.BrightnessValue = lle;
                }
                else if (command.Contains("LLN"))
                {//Уровень яркости для ночного режима
                    var max = HomePageViewModel.Default.BrightnessNightMaxValue;
                    var min = HomePageViewModel.Default.BrightnessNightMinValue;

                    var lln = byte.Parse(new Regex(@"LLN:(\d{1,3})").Match(command).Groups[1].Value);

                    if (lln > max)
                    {
                        lln = max;
                    }
                    if (lln < min)
                    {
                        lln = min;
                    }

                    HomePageViewModel.Default.BrightnessNightValue = lln;
                }
                else if (command.Contains("LLM"))
                {//Максимальная яркость ленты
                    Regex regex = new Regex(@"LLM:(\d{1,3})");

                    HomePageViewModel.Default.BrightnessMaxValue =
                        byte.Parse(regex.Match(command).Groups[1].Value);
                    HomePageViewModel.Default.BrightnessNightMaxValue =
                        byte.Parse(regex.Match(command).Groups[1].Value);

                    ThinSettingsPageViewModel.Default.MaximumBrightness =
                        byte.Parse(regex.Match(command).Groups[1].Value);


                }
                else if (command.Contains("LLI"))
                {//Минимальная яркость ленты
                    Regex regex = new Regex(@"LLI:(\d{1,3})");

                    HomePageViewModel.Default.BrightnessMinValue =
                        byte.Parse(regex.Match(command).Groups[1].Value);
                    HomePageViewModel.Default.BrightnessNightMinValue =
                        byte.Parse(regex.Match(command).Groups[1].Value);
                    ThinSettingsPageViewModel.Default.MinimumBrightness =
                        byte.Parse(regex.Match(command).Groups[1].Value);
                }
                else if (command.Contains("AOM"))
                {
                    //Тип автовыключения: полное или ночник
                    ThinSettingsPageViewModel.Default.ComboboxIndex =
                        byte.Parse(new Regex(@"AOM:([0-1])").Match(command).Groups[1].Value);
                }
                else if (command.Contains(@"TML"))
                {
                    var temperature = byte.Parse(new Regex(@"TML:(\d{3})").Match(command).Groups[1].Value);
                    if (temperature > 100)
                        ThinSettingsPageViewModel.Default.TapeTemperature = 100;
                    else if (temperature < 0)
                        ThinSettingsPageViewModel.Default.TapeTemperature = 0;
                    else
                        ThinSettingsPageViewModel.Default.TapeTemperature = temperature;

                }
                else if (command.Contains(@"TMO"))
                {
                    ThinSettingsPageViewModel.Default.SwitchOffTemp =
                        byte.Parse(new Regex(@"TML:(\d{2})").Match(command).Groups[1].Value);
                }
                else if (command.Contains("DFV"))
                {
                    _flag++;
                }
                else if (command.Contains("DRV"))
                {
                    _flag++;
                }
                else if (command.Contains("DBG"))
                {
                    _flag++;
                }
            }
        }

        private void ParseTimeFromInt(string ats)
        {
            var timeConverter = new TimeConverter();
            if (ats.Contains("ats"))
            {

                HomePageViewModel.Default.HourWeekend = timeConverter.GetTimeHour(int.Parse(new Regex(@"ats(\d{1,4})").Match(ats).Groups[1].Value)).ToString();
                HomePageViewModel.Default.MinuteWeekend = timeConverter.GetTimeMinute(int.Parse(new Regex(@"ats(\d{1,4})").Match(ats).Groups[1].Value)).ToString();
            }
            else
            {
                HomePageViewModel.Default.HourDays = timeConverter.GetTimeHour(int.Parse(new Regex(@"atw(\d{1,4})").Match(ats).Groups[1].Value)).ToString();
                HomePageViewModel.Default.MinuteDays = timeConverter.GetTimeMinute(int.Parse(new Regex(@"atw(\d{1,4})").Match(ats).Groups[1].Value)).ToString();

            }
        }
    }
}
