﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Services
{
    public class TimeConverter
    {
        public int GetTimeHour(int time)
        {
            return time / 60;
        }

        public int GetTimeMinute(int time)
        {
            return time % 60;
        }
        public string GetDate(int year, int month, int day)
        {
            string _year = year.ToString();
            string _month = month.ToString();
            string _day = day.ToString();

            if (_month.Length <2)
            {
                _month = "0" + _month;
            }
            if (_day.Length < 2)
            {
                _day = "0" + _day;
            }

            return _year + _month + _day;

        }
    }
}
