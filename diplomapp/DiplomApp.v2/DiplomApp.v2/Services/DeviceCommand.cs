﻿using DiplomApp.v2.Interfaces;
using DiplomApp.v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Services
{
    public class DeviceCommand<TResponse> : IDeviceCommand<TResponse>
    {
        string _commandName;
        public string CommandName
        {
            get => _commandName;
            set
            {
                if (value == _commandName) return;
                _commandName = value;
            }
        }
    }
}
