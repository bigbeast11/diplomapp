﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Interfaces
{
    public interface IDeviceCommand<TResponse>
    {
        string CommandName { get; set; }
    }
}
