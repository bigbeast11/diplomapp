﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Interfaces
{
    public interface IConnection
    {
        bool IsConnect { get; set; }
        void Connect(string deviceAdress);
        void Disconnect();
        string ReadData();
        void WriteData(string data);
    }
}
