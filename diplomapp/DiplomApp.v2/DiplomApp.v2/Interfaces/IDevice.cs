﻿using DiplomApp.v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Interfaces
{
    public interface IDevice
    {
        string DeviceAdress { get; set; }
        bool IsConnected { get; set; }
        void Connect(string deviceAdress);
        void Disconnect();
        void SendCommand<TResponse>(IDeviceCommand<TResponse> command);
    }
}
