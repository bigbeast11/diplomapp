﻿using DiplomApp.v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Interfaces
{
    public interface IDeviceResponseParser
    {
        TResponse Parse<TResponse>(string raw) where TResponse : CommandModel;
    }
}
