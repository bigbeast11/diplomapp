﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class ViewViewModelMap : IViewViewModelMap
    {
        readonly IViewProvider _viewProvider;
        readonly Dictionary<Type, Type> _map = new Dictionary<Type, Type>();

        public ViewViewModelMap(IViewProvider viewProvider)
        {
            _viewProvider = viewProvider;
        }
        public void Bind<TView, TViewModel>()
            where TView : IView
            where TViewModel : IViewModel
        {
            _map[typeof(TViewModel)] = typeof(TView);
        }

        public object GetView<TViewModel>() where TViewModel : IViewModel
        {
            return _viewProvider.GetView(_map[typeof(TViewModel)]);
        }

        public IView GetView(Type viewModelType)
        {
            return (IView)_viewProvider.GetView(_map[viewModelType]);
        }
    }
}
