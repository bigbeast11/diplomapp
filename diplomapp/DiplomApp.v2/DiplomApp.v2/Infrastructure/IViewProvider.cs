﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IViewProvider
    {
        TView GetView<TView>() where TView : IView;
        object GetView(Type viewType);
    }
}
