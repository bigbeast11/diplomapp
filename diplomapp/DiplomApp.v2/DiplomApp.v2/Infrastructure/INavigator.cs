﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DiplomApp.v2.Infrastructure
{
    public interface INavigator
    {
        void SetContext(INavigation navigationContext);
        Task<TViewModel> NavigateTo<TViewModel>(Action<TViewModel> setStateAction = null) where TViewModel : IViewModel;
       
    }
}
