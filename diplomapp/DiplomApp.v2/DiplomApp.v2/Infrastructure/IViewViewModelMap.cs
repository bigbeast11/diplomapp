﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public interface IViewViewModelMap
    {
        void Bind<TView, TViewModel>()
            where TView : IView
            where TViewModel : IViewModel;

        object GetView<TViewModel>() where TViewModel : IViewModel;
        IView GetView(Type viewModelType);
    }
}
