﻿using Autofac;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Infrastructure
{
    public class ViewModelProvider : IViewModelProvider
    {
        private readonly IComponentContext _container;

        public ViewModelProvider(IComponentContext container)
        {
            _container = container;
        }
        public TViewModel GetViewModel<TViewModel>() where TViewModel : IViewModel
        {
            return _container.Resolve<TViewModel>();
        }
        public IViewModel GetViewModel(Type typeOfView)
            => (IViewModel)_container.Resolve(typeOfView);
    }
}
