﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;
using Xamarin.Forms;

namespace DiplomApp.v2.Infrastructure
{
    public class Navigator : INavigator
    {
        private readonly IViewViewModelMap _map;
        private readonly IViewModelProvider _viewModelProvider;
        private INavigation _navigation;

        public Navigator(IViewViewModelMap map, IViewModelProvider viewModelProvider)
        {
            _map = map;
            _viewModelProvider = viewModelProvider;
        }

        public async Task<TViewModel> NavigateTo<TViewModel>(Action<TViewModel> setStateAction = null) where TViewModel : IViewModel
        {
            Page view = _map.GetView<TViewModel>() as Page;

            if (view == null) throw new InvalidOperationException();

            TViewModel viewModel = _viewModelProvider.GetViewModel<TViewModel>();

            if (setStateAction != null) setStateAction(viewModel);
            view.BindingContext = viewModel;

            await _navigation.PushAsync(view);

            return viewModel;
        }

        public void SetContext(INavigation navigationContext)
        {
            if (navigationContext == null) throw new ArgumentNullException(nameof(navigationContext));
            _navigation = navigationContext;
        }
    }
}
