﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Infrastructure
{
    public interface IViewModelProvider
    {
        TViewModel GetViewModel<TViewModel>() where TViewModel : IViewModel;
        IViewModel GetViewModel(Type typeOfView);
    }
}
