﻿using Autofac;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomApp.v2.Infrastructure
{
    public class ViewProvider : IViewProvider
    {
        readonly IComponentContext _container;

        public ViewProvider(IComponentContext container)
        {
            _container = container;
        }
        public TView GetView<TView>() where TView : IView
        {
            return _container.Resolve<TView>();
        }

        public object GetView(Type viewType)
        {
            return _container.Resolve(viewType);
        }
    }
}
